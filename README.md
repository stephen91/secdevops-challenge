# SecDevOps Challenge

Our AWS environment hosts a number of micro-services against which we run tests to ensure their compliance with security requirements and to detect any changes with their associative resources. It is necessary to create a REST API that will interact with the underlying database which is Amazon S3.  

## Prerequisites

- [ ] Install Java 13 JDK
- [ ] Create AWS account (https://aws.amazon.com/free)
- [ ] Install and configure AWS CLI (https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/welcome.html)
- [ ] Install Maven (https://maven.apache.org/download.cgi)


## Operating Instructions

- [ ] Run the command 'mvn spring-boot:run' in the top level directory of the repository.
- [ ] To log in use 'user' as the username and the password will be generated to the console log.
- [ ] Access the REST API at 'http://localhost:8181' followed by the URI of the resource you wish to access. 
	e.g. 'http://localhost:8181/buckets/{bucketName}/encryptStatus' will give you the Encryption Type of the specified bucket. Note will return Error if the bucket is not encrypted. 
	
## Built With

- Maven: Build tool and Dependency Management
- Spring Framework: Enterprise application framework 
- Spring Boot: Simplifies configuration of Spring framework.

## Author
Stephen O'Brien
