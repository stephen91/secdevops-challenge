package com.challenge.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;


@RunWith(MockitoJUnitRunner.class)
public class S3ClientControllerTest {
	
	@InjectMocks
	private S3ClientController s3Client = new S3ClientController();
	
	@Mock
	private AmazonS3 mockS3;
	@Mock
	private Bucket mockBucket;
	
	private List<Bucket> bucketList;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		bucketList = new ArrayList<Bucket>();
		bucketList.add(mockBucket);
		Mockito.when(mockS3.listBuckets()).thenReturn(bucketList);
	}

	@Test
	public void listOneBucket() {
		assertEquals(bucketList, s3Client.listAllBuckets());
	}

}
