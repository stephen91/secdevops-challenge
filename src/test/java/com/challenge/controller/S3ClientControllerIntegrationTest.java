package com.challenge.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.challenge.SecDevOpsChallengeApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SecDevOpsChallengeApplication.class})
public class S3ClientControllerIntegrationTest {
	
	private S3ClientController s3Client = new S3ClientController();

	@Test(expected = AmazonS3Exception.class)
	public void noEncryptionOnBucket_returns404StatusCode() {
		s3Client.getEncryptionStatus("secdevops2");
	}

}
