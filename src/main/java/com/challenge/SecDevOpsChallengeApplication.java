package com.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecDevOpsChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecDevOpsChallengeApplication.class, args);
	}

}
