package com.challenge.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetBucketEncryptionResult;
import com.amazonaws.services.s3.model.ListBucketInventoryConfigurationsRequest;
import com.amazonaws.services.s3.model.ListBucketInventoryConfigurationsResult;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;

@RestController
public class S3ClientController {
	
	private AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
	
	@GetMapping("/buckets")
	public List<Bucket> listAllBuckets() {
		List<Bucket> buckets = s3Client.listBuckets();
		return buckets;
	}
	
	@GetMapping("/buckets/{bucketName}/objects")
	public List<S3ObjectSummary> listObjectsInBucket(@PathVariable String bucketName){
		ListObjectsV2Result objectsInBucket = s3Client.listObjectsV2(bucketName);
		return objectsInBucket.getObjectSummaries();
	}
	
	@GetMapping("/buckets/{bucketName}/encryptStatus")
	public GetBucketEncryptionResult getEncryptionStatus(@PathVariable String bucketName) {
		return s3Client.getBucketEncryption(bucketName);
	}
	
	@GetMapping("/objects")
	public List<ListObjectsV2Result> getAllObjects(){
		List<ListObjectsV2Result> allObjects = new ArrayList<ListObjectsV2Result>();
		for(Bucket bucket : listAllBuckets()) {
			allObjects.add(s3Client.listObjectsV2(bucket.getName()));
		}
		return allObjects;
	}
	
	@GetMapping("/objects/{encryptStatus}")
	public ListBucketInventoryConfigurationsResult getObjectsByEncryptionType(@PathVariable String encryptStatus){
		List<Bucket> buckets = listAllBuckets();
		Map<String, String> bucketName2Encryption = new HashMap<String, String>();
		ListBucketInventoryConfigurationsResult listBucketInventoryConfigurations = null;
		for(Bucket bucket : buckets) {
		}
		return null;
	}
	
}
